import React from 'react';
import './App.css';
import LoginPage from './LoginPageComp';

function App() {
  return (
    <div>
      <LoginPage />
    </div>
  );
}

export default App;
