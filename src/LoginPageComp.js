import React, { useState } from "react";
import './App.css';
import axios from 'axios';

function LoginPage() {
  const [username, setUsername] = useState("string");
  const [password, setPassword] = useState("string");
  const [isAuth, setIsAuth] = useState(false);

  function login() {
    const form = new FormData();
    form.append('grant_type', "password");
    form.append('username', username);
    form.append('password', password);

    axios.post(`https://localhost:7241/token`, form, { withCredentials: true })
      .then(res => {
        setIsAuth(true);
      })
  }

  function onUsernameChange(e) {
    setUsername(e.target.value);
  }

  function onPasswordChange(e) {
    setPassword(e.target.value);
  }

  return (
    <div className="App">
      {!isAuth && <div className='{}'>
        <input value={username} onChange={onUsernameChange}></input>
        <input value={password} onChange={onPasswordChange}></input>
        <button onClick={login}>Login</button>
      </div>}
    </div>
  );
}

export default LoginPage;